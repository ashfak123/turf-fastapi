from typing import List
from fastapi import APIRouter, Depends
from app.utils import auth as auth_
from app.schemas import slots
from app.crud import slots as crud

router = APIRouter(prefix='/slots', tags=['Slots'])
auth = auth_.AuthHandler()


@router.get('/', response_model=List[slots.SlotIn])
async def list_all_slots(user_id=Depends(auth.auth_wrapper)):
    all_slots = await crud.list_slots(user_id)
    return all_slots


@router.post('/')
async def create_slots(slot: slots.SlotIn, user_id=Depends(auth.auth_wrapper)):
    new_slot = await crud.create_slots(user_id, **slot.dict())
    return new_slot.id
