from fastapi import APIRouter, HTTPException
from app.crud import users
from app.schemas import users as user_schema
from app.utils import auth as auth_

auth = auth_.AuthHandler()

router = APIRouter(prefix='/user', tags=['User'])


@router.post('/register')
async def register_owner(body: user_schema.OwnerIn):
    body_dict = body.dict()
    credentials = body_dict.pop('user')
    hash_password = auth.get_password_hash(credentials['password'])
    user = await users.create_user(username=credentials['username'], password_hash=hash_password)
    owner_id = await users.create_owner(user_id=user, **body_dict)
    return owner_id


@router.post('/login')
async def owner_login(creds: user_schema.UserIn):
    user = await users.get_user(creds.username)
    if not user:
        raise HTTPException(status_code=401, detail='Invalid username and/or password')
    valid_creds = auth.verify_password(creds.password, user.password_hash)
    if not valid_creds:
        raise HTTPException(status_code=401, detail='Invalid password')
    return {
        'token': auth.encode_token(user.id)
    }
