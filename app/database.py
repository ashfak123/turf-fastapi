from fastapi import FastAPI
from tortoise.contrib.fastapi import register_tortoise
from .settings import db_url


TORTOISE_ORM = {
    "connections": {"default": db_url},
    "apps": {
        "models": {
            "models": [
                "models.users",
                "aerich.models",
                "models.slots"
            ],
            "default_connection": "default",
        },
    },
}


def init_db(app: FastAPI) -> None:
    register_tortoise(
        app,
        db_url=db_url,
        modules={"models": [
            "app.models.users",
            "app.models.slots"
        ]},
        generate_schemas=True,
        add_exception_handlers=True,
    )
