-- upgrade --
CREATE TABLE IF NOT EXISTS "user" (
    "id" SERIAL NOT NULL PRIMARY KEY,
    "username" VARCHAR(40) NOT NULL UNIQUE,
    "first_name" VARCHAR(25),
    "last_name" VARCHAR(25),
    "email" VARCHAR(20) NOT NULL,
    "password_hash" VARCHAR(200) NOT NULL,
    "created_at" TIMESTAMPTZ NOT NULL  DEFAULT CURRENT_TIMESTAMP
);
CREATE TABLE IF NOT EXISTS "owner" (
    "id" SERIAL NOT NULL PRIMARY KEY,
    "title" VARCHAR(50) NOT NULL,
    "address" VARCHAR(100) NOT NULL,
    "rating" DOUBLE PRECISION,
    "review_count" INT,
    "user_id" INT NOT NULL UNIQUE REFERENCES "user" ("id") ON DELETE CASCADE
);
CREATE TABLE IF NOT EXISTS "aerich" (
    "id" SERIAL NOT NULL PRIMARY KEY,
    "version" VARCHAR(255) NOT NULL,
    "app" VARCHAR(20) NOT NULL,
    "content" JSONB NOT NULL
);
CREATE TABLE IF NOT EXISTS "slotsinturf" (
    "id" SERIAL NOT NULL PRIMARY KEY,
    "time_start" VARCHAR(20) NOT NULL,
    "time_end" VARCHAR(20) NOT NULL,
    "owner_id" INT NOT NULL REFERENCES "owner" ("id") ON DELETE CASCADE
);
