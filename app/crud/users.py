from app.models.users import User, Owner
from app.utils import auth as auth_

auth = auth_.AuthHandler()


async def create_user(**kwargs):
    user = await User.create(**kwargs, email='email')
    return user.id


async def create_owner(**kwargs):
    owner = await Owner.create(**kwargs)
    return owner.id


async def get_user(username: str) -> User:
    user = await User.get_or_none(username=username)
    return user
