from app.models.slots import SlotsInTurf


async def create_slots(user: int, **kwargs):
    slot = await SlotsInTurf.create(owner_id=user, **kwargs)
    return slot


async def list_slots(user: int):
    slots = await SlotsInTurf.filter(owner_id=user)
    return slots
