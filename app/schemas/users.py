from pydantic import BaseModel


class UserIn(BaseModel):
    username: str
    password: str


class OwnerIn(BaseModel):
    title: str
    address: str
    user: UserIn


class OwnerOut(BaseModel):
    title: str
    id: int
