from pydantic import BaseModel
from datetime import time


class SlotIn(BaseModel):
    time_start: time
    time_end: time
