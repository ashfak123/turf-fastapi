from pydantic import BaseModel


class Customer(BaseModel):
    first_name = str
    last_name = str
    phone = str
    address = str
