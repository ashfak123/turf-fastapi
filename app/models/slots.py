from tortoise.models import Model
from tortoise import fields
from tortoise.contrib.pydantic import pydantic_model_creator


class SlotsInTurf(Model):
    owner = fields.ForeignKeyField(model_name='models.Owner', on_delete=fields.CASCADE, related_name='slots')
    time_start = fields.CharField(max_length=20)
    time_end = fields.CharField(max_length=20)


slots_pydantic_schema = pydantic_model_creator(SlotsInTurf)
