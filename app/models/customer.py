from tortoise.models import Model
from tortoise import fields


class Customer(Model):
    user = fields.OneToOneField('models.User', on_delete=fields.CASCADE, related_name='user')
    address = fields.CharField(max_length=200)
