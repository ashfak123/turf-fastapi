from tortoise.models import Model
from tortoise import fields
from tortoise.contrib.pydantic import pydantic_model_creator


class User(Model):
    username = fields.CharField(max_length=40, unique=True)
    first_name = fields.CharField(max_length=25, null=True)
    last_name = fields.CharField(max_length=25, null=True)
    email = fields.CharField(max_length=20)
    password_hash = fields.CharField(max_length=200)
    created_at = fields.DatetimeField(auto_now_add=True)

    def __str__(self) -> str:
        return self.username

    class PydanticMeta:
        exclude = ["email", "first_name", "last_name"]


class Owner(Model):
    user = fields.OneToOneField(model_name='models.User', related_name='user', on_delete=fields.CASCADE)
    title = fields.CharField(max_length=50, required=True)
    address = fields.CharField(max_length=100)
    rating = fields.FloatField(null=True)
    review_count = fields.IntField(null=True)

    def __str__(self) -> str:
        return self.title


user_pydantic = pydantic_model_creator(User, name="User")
