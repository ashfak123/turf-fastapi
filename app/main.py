from fastapi import FastAPI
from app.routers import users
from app.routers import slots
from .database import init_db


app = FastAPI()

app.include_router(users.router)
app.include_router(slots.router)


@app.on_event("startup")
async def startup_event():
    print("Starting up...")
    init_db(app)
