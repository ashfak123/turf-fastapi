FROM python:3.7

COPY ./app /app
COPY ./requirements.txt /app
WORKDIR /app
RUN pip install --no-cache-dir -r requirements.txt